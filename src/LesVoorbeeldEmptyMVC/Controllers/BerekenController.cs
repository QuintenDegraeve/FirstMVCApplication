﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using myBerekeningen;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace LesVoorbeeldEmptyMVC.Controllers
{
    public class BerekenController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Faculteit(string id)
        {
            ViewBag.Route = "Faculteit";
            ViewBag.Faculteit = id;
            if (!string.IsNullOrEmpty(id)) {
                Faculteit f = new Faculteit(Int32.Parse(id));
                ViewBag.Resultaat = $"De faculteit van {id} is {f.Bereken()}.";
            }
            return View("Index");
        }
        public IActionResult Temperatuur(string param)
        {
            ViewBag.Route = "Temperatuur";
            string type = param.Split('/')[0];
            double waarde = double.Parse(param.Split('/')[1]);

            if (!string.IsNullOrEmpty(param))
            {
                Temperatuur t = new Temperatuur(waarde);
                switch (type)
                {
                    case "inKelvin":
                        ViewBag.Resultaat = $"De temperatuur van {waarde} Celsius is {t.KelvinNumber} in Kelvin.";
                        break;
                    case "inFahrenheit":
                        ViewBag.Resultaat = $"De temperatuur van {waarde} Celsius is {t.FahrenheitNumber} in Fahrenheit.";
                        break;
                }
            }
            return View("Index");
        }
    }
}
