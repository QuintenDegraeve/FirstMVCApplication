﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyHobbies;
// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace LesVoorbeeldEmptyMVC.Controllers
{
    public class Band : Controller
    {
        // GET: /<controller>/
        public string Index()
        {
            return "Welkom bij uw Band app. Hier komt u alles te weten over de bands en hun leden";
        }

        public IActionResult Lijst()
        {
            List<MyHobbies.MusicBand> bands = new List<MyHobbies.MusicBand>();
            bands.Add(new MusicBand("The C Sharpers", 1985));
            bands.Add(new MusicBand("Java With Beans", 1999));
            ViewBag.BandList = bands;
            return View();
        }

        public IActionResult LijstMetLeden()
        {

            return View();
        }
    }
}
