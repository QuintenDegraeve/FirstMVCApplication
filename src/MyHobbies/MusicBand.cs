﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyHowest;

namespace MyHobbies
{
    public class MusicBand
    {
        public enum Geslacht
        {
            Man,
            Vrouw
        }

        public string Naam { get; set; }
        public int Jaar { get; set; }
        public List<Bandlid> Bandleden { get; set; }

        public MusicBand(string naam, int jaar)
        {
            this.Naam = naam;
            this.Jaar = jaar;
            Bandleden = new List<Bandlid>();
            Bandleden.Add(new Bandlid("Man van Manster", 1960, Geslacht.Man));
            Bandleden.Add(new Bandlid("Vrouw der Vrouwster", 1962, Geslacht.Vrouw));

        }

    }

    public class Bandlid
    {
        public string Naam { get; set; }
        public int Jaar { get; set; }
        public Enum Geslacht { get; set; }

        public Bandlid(string naam, int jaar, Enum geslacht)
        {
            this.Naam = naam;
            this.Jaar = jaar;
            this.Geslacht = geslacht;
        }
    }
}
