﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myBerekeningen
{
    public class Temperatuur
    {
        public double CelsiusNumber { get; set; }
        public double FahrenheitNumber { get; }
        public double KelvinNumber { get; }

        //Of zet de berekening in de getters voor Fahrenheit en Kelvin.
        //Zie oplossingen!

        public Temperatuur(double celsius)
        {
            this.CelsiusNumber = celsius;
            this.FahrenheitNumber = (celsius * 1.8) + 32;
            this.KelvinNumber = celsius + 273.15;
        }
    }
}
