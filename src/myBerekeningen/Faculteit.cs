﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myBerekeningen
{
    public class Faculteit
    {
        public int Number { get; set; }
        public int Result { get; set; }
        public Faculteit(int number)
        {
            this.Number = number;
            this.Result = this.Number;
        }

        public int Bereken()
        {
            while (this.Number >= 2)
            {
                this.Number--;
                this.Result *= this.Number;
            }
            return this.Result;
        }
    }
}
