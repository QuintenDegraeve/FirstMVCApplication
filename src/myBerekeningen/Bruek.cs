﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myBerekeningen
{
    public class Breuk
    {
        public int Teller { get; set; }
    public int Noemer { get; set; }

    public Breuk(int teller, int noemer)
    {
        this.Teller = teller;
        this.Noemer = noemer;
    }

    public override string ToString()
    {
        return this.Teller + "/" + this.Noemer;
    }

    public static Breuk operator +(Breuk b1, Breuk b2)
    {
        int LCM = lcm(b1.Noemer, b2.Noemer);
        int b1TellerMultiply = LCM / b1.Noemer;
        int b2TellerMultiply = LCM / b2.Noemer;

        //Andere manier staat in de oplossingen.
        return new Breuk((b1.Teller * b1TellerMultiply) + (b2.Teller * b2TellerMultiply), LCM);
    }

    public static Breuk operator -(Breuk b1, Breuk b2)
    {
        int LCM = lcm(b1.Noemer, b2.Noemer);
        int b1TellerMultiply = LCM / b1.Noemer;
        int b2TellerMultiply = LCM / b2.Noemer;

        return new Breuk((b1.Teller * b1TellerMultiply) - (b2.Teller * b2TellerMultiply), LCM);
    }

    public static Breuk operator *(Breuk b1, Breuk b2)
    {
        return new Breuk(b1.Teller * b2.Teller, b1.Noemer * b2.Noemer);
    }

    public static Breuk operator /(Breuk b1, Breuk b2)
    {
        return new Breuk(b1.Teller * b2.Noemer, b1.Noemer * b2.Teller);
    }

    public static bool operator !=(Breuk b1, Breuk b2)
    {
        return ((b1.Teller / b1.Noemer) != (b2.Teller / b2.Noemer));
    }

    public static bool operator ==(Breuk b1, Breuk b2)
    {
        return ((b1.Teller / b1.Noemer) == (b2.Teller / b2.Noemer));
    }

    public static bool operator >(Breuk b1, Breuk b2)
    {
        return ((b1.Teller / b1.Noemer) > (b2.Teller / b2.Noemer));
    }

    public static bool operator <(Breuk b1, Breuk b2)
    {
        return ((b1.Teller / b1.Noemer) < (b2.Teller / b2.Noemer));
    }

    //LCM -- Math
    private static int lcm(int a, int b)
    {
        return (a / gcf(a, b)) * b;
    }

    private static int gcf(int a, int b)
    {
        while (b != 0)
        {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
}
}
